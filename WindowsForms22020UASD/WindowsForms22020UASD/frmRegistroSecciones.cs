﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using INF518Core.Clases;
using INF518Core;

namespace WindowsForms22020UASD
{
    public partial class frmRegistroSecciones : Form
    {
        Mantenimiento mant;
        private readonly Usuario _usuario;
        public frmRegistroSecciones(int Id, Usuario usuario)
        {
            InitializeComponent();
            mant = new Mantenimiento(usuario);
            _usuario = usuario;
            CargarCarreras();
            CargarProfesores();
            CargarCentros();
            Dia1.SelectedIndex = 0;
            Dia2.SelectedIndex = 0;
        }


        void CargarAulas(int idCentro)
        {
            cbbAula.DataSource = mant.GetListadoAulas(idCentro);
            cbbAula.ValueMember = "Id";
            cbbAula.DisplayMember = "Codigo";
        }
        void CargarAsignaturas(int idCarrera)
        {
            cbbAsignatura.DataSource = mant.GetListadoAsignaturas(idCarrera);
            cbbAsignatura.ValueMember = "Id";
            cbbAsignatura.DisplayMember = "Descripcion";
        }
        void CargarProfesores()
        {
            cbbProfesor.DataSource = mant.GetListadoProfesores();
            cbbProfesor.ValueMember = "IdProfesor";
            cbbProfesor.DisplayMember = "Nombre";
        }
        void CargarCentros()
        {
            cbbCentros.DataSource = mant.GetListadoCentros();
            cbbCentros.ValueMember = "Id";
            cbbCentros.DisplayMember = "Descripcion";
        }
        void CargarCarreras()
        {
            cbbCarrera.DataSource = mant.GetListadoCarreras();
            cbbCarrera.ValueMember = "Id";
            cbbCarrera.DisplayMember = "Nombre";
        }

        private void cbbCentros_SelectedValueChanged(object sender, EventArgs e)
        {
            int idCentro = 0;
            int.TryParse(cbbCentros.SelectedValue.ToString(), out idCentro);
            if(idCentro>0)
            {
                CargarAulas(idCentro);
            }
        }

        private void cbbCarrera_SelectedValueChanged(object sender, EventArgs e)
        {
            int idCarrera = 0;
            int.TryParse(cbbCarrera.SelectedValue.ToString(), out idCarrera);
            if (idCarrera > 0)
            {
                CargarAsignaturas(idCarrera);
            }
        }
    }
}
