﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using INF518Core;
using INF518Core.Clases;
using INF518Core.Utilitarios;

namespace WindowsForms22020UASD
{
    public partial class frmListadoGruposUsuarios : Form
    {
        Mantenimiento mant;
        DataTable dt;
        private readonly Usuario _usuario;
        public frmListadoGruposUsuarios(Usuario usuario)
        {
            InitializeComponent();
            _usuario = usuario;
            mant = new Mantenimiento(_usuario);
            dt = new DataTable();
            NewID = 0;
        }

        public int NewID { get; set; }
        private void btnNuevo_Click(object sender, EventArgs e)
        {

            frmRegistrosGruposUsuarios frm = new frmRegistrosGruposUsuarios(0,_usuario);
            frm.StartPosition = FormStartPosition.CenterScreen;
            if(frm.ShowDialog()==DialogResult.OK)
            {

            }
            frm.Dispose();
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            //Verifica si el datagridview tiene filas
            if (dgvListadoTipoUsuarios.Rows.Count > 0)
            {
                int Id = 0;
                //Esta linea consigue el Id del datagridview para enviarlo al formulario.
                int.TryParse(dgvListadoTipoUsuarios.SelectedRows[0].Cells["colId"].Value.ToString(), out Id);
                if (Id > 0) //Solo carga el formulario si el Id es mayor que 0
                {

                    frmRegistrosGruposUsuarios frm = new frmRegistrosGruposUsuarios(Id, _usuario);
                    frm.StartPosition = FormStartPosition.CenterScreen;
                    if (frm.ShowDialog() == DialogResult.OK)
                    {
                        NewID = Id; //Asigna NewID, el Id para modificar.
                        btnBuscar_Click(sender, e);
                    }
                    frm.Dispose();
                }
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            dt = mant.GetListadoTipoUsuarios(ConstruirCriterio());
            //No permite que las columnas se carguen directo del query, se hará con las columnas que se carga explícitamente.
            this.dgvListadoTipoUsuarios.AutoGenerateColumns = false;
            this.dgvListadoTipoUsuarios.DataSource = dt; //asigna el datatatble al datagridview
            if (dt != null && dt.Rows.Count == 0)
            {
                this.dgvListadoTipoUsuarios.DataSource = null;
            }
            ActualizarTotal();
        }
        void ActualizarTotal()
        {
            NewID = 0; //Limpia el Id para futuras busquedas
            this.lblTotal.Text = dt.DefaultView.Count.ToString();
            if (dgvListadoTipoUsuarios.DataSource == null)
                this.lblTotal.Text = string.Empty;
        }
        string ConstruirCriterio()
        {
            StringBuilder criterio = new StringBuilder();
            if (NewID == 0)
            {
                criterio.Append(" u.Id>0 ");
            }
            else
            {
                criterio.AppendFormat(" u.Id={0}", NewID);
            }
            if (txtNombre.Text.Trim().Length > 0)
                criterio.AppendFormat(" AND u.Nombre Like '%{0}%' ", txtNombre.Text);

            return criterio.ToString();
        }

        private void txtNombre_TextChanged(object sender, EventArgs e)
        {
            ///Para filtrar solo los registros del datatable que ya está cargado
            if (dt != null && dt.Rows.Count > 0)
            {
                this.dt.DefaultView.RowFilter = ConstruirCriterio();
                ActualizarTotal();
            }
        }

        private void toolStripBuscar_Click(object sender, EventArgs e)
        {
            btnBuscar_Click(sender, e);
        }

        private void toolStripNuevo_Click(object sender, EventArgs e)
        {
            btnNuevo_Click(sender, e);
        }

        private void toolStripModificar_Click(object sender, EventArgs e)
        {
            btnModificar_Click(sender, e);
        }
    }
}
