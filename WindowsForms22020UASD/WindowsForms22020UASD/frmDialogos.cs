﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsForms22020UASD
{
    public partial class btnFuente : Form
    {
        public btnFuente()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnColorFondo_Click(object sender, EventArgs e)
        {
            if(colorDialog1.ShowDialog()==DialogResult.OK)
            {
                this.txtColores.BackColor = colorDialog1.Color;
            }
        }

        private void btnColorFrente_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                this.txtColores.ForeColor = colorDialog1.Color;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (fontDialog1.ShowDialog() == DialogResult.OK)
            {
                this.txtColores.Font = fontDialog1.Font;
                this.txtColores.ForeColor = fontDialog1.Color;
            }
        }

        private void fontDialog1_Apply(object sender, EventArgs e)
        {
            this.txtColores.Font = fontDialog1.Font;
            this.txtColores.ForeColor = fontDialog1.Color;
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "Archivos rtf | *.rtf";
            if (saveFileDialog1.ShowDialog()==DialogResult.OK)
            {
                this.richTextBox1.SaveFile(saveFileDialog1.FileName);
            }
        }

        private void btnAbrir_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Archivos rtf | *.rtf";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                this.richTextBox1.LoadFile(openFileDialog1.FileName);
            }
        }

        private void copiarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.richTextBox1.Copy();
        }

        private void cortarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.richTextBox1.Cut();
        }

        private void pegarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.richTextBox1.Paste();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Imagénes (jpg, png) | *.jpg; *.png ";
            Image img;
            if (openFileDialog1.ShowDialog()==DialogResult.OK)
            {
                img = Image.FromFile(openFileDialog1.FileName);
                Clipboard.SetImage(img);
                richTextBox1.Paste();
                Clipboard.Clear();
            }
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {

            if (richTextBox1.SelectionFont == null)
            {
                //para prevenir quitar estilos que ya tenga la seleccion
                return;
            }
            //consiguer el estilo actual
            FontStyle style = richTextBox1.SelectionFont.Style;
            //Adjustando segun lo requerido
            if (richTextBox1.SelectionFont.Bold)
                style &= ~FontStyle.Bold;
            else
                style |= FontStyle.Bold;

            ///Asignar el nuevo estilo
            richTextBox1.SelectionFont = new Font(richTextBox1.SelectionFont, style);
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            if (richTextBox1.SelectionFont == null)
            {
                //para prevenir quitar estilos que ya tenga la seleccion
                return;
            }
            //consiguer el estilo actual
            FontStyle style = richTextBox1.SelectionFont.Style;
            //Adjustando segun lo requerido
            if (richTextBox1.SelectionFont.Italic)
                style &= ~FontStyle.Italic;
            else
                style |= FontStyle.Italic;

            ///Asignar el nuevo estilo
            richTextBox1.SelectionFont = new Font(richTextBox1.SelectionFont, style);
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            if (richTextBox1.SelectionFont == null)
            {
                //para prevenir quitar estilos que ya tenga la seleccion
                return;
            }
            //consiguer el estilo actual
            FontStyle style = richTextBox1.SelectionFont.Style;
            //Adjustando segun lo requerido
            if (richTextBox1.SelectionFont.Underline)
                style &= ~FontStyle.Underline;
            else
                style |= FontStyle.Underline;

            ///Asignar el nuevo estilo
            richTextBox1.SelectionFont = new Font(richTextBox1.SelectionFont, style);
        }

        private void copyToolStripButton_Click(object sender, EventArgs e)
        {
            copiarToolStripMenuItem_Click(sender, e);
        }

        private void cutToolStripButton_Click(object sender, EventArgs e)
        {
            cortarToolStripMenuItem_Click(sender, e);
        }

        private void pasteToolStripButton_Click(object sender, EventArgs e)
        {
            pegarToolStripMenuItem_Click(sender, e);
        }
    }
}
