﻿namespace WindowsForms22020UASD
{
    partial class frmRegistroSecciones
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.horaFinalDia2 = new System.Windows.Forms.DateTimePicker();
            this.horaInicialDia2 = new System.Windows.Forms.DateTimePicker();
            this.Dia2 = new System.Windows.Forms.ComboBox();
            this.horaFinalDia1 = new System.Windows.Forms.DateTimePicker();
            this.horaInicialDia1 = new System.Windows.Forms.DateTimePicker();
            this.Dia1 = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.txtId = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cbbAsignatura = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbbProfesor = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbbAula = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cbbCentros = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cbbCarrera = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCancelar
            // 
            this.btnCancelar.ForeColor = System.Drawing.Color.Red;
            this.btnCancelar.Location = new System.Drawing.Point(94, 295);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 47;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(13, 295);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(75, 23);
            this.btnGuardar.TabIndex = 46;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.cbbCarrera);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.cbbCentros);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.cbbAula);
            this.groupBox1.Controls.Add(this.horaFinalDia2);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.horaInicialDia2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.Dia2);
            this.groupBox1.Controls.Add(this.cbbProfesor);
            this.groupBox1.Controls.Add(this.horaFinalDia1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.horaInicialDia1);
            this.groupBox1.Controls.Add(this.cbbAsignatura);
            this.groupBox1.Controls.Add(this.Dia1);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtId);
            this.groupBox1.Controls.Add(this.numericUpDown1);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(454, 276);
            this.groupBox1.TabIndex = 48;
            this.groupBox1.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 242);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 13);
            this.label6.TabIndex = 66;
            this.label6.Text = "Día 2";
            // 
            // horaFinalDia2
            // 
            this.horaFinalDia2.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.horaFinalDia2.Location = new System.Drawing.Point(330, 237);
            this.horaFinalDia2.Name = "horaFinalDia2";
            this.horaFinalDia2.ShowUpDown = true;
            this.horaFinalDia2.Size = new System.Drawing.Size(113, 20);
            this.horaFinalDia2.TabIndex = 65;
            // 
            // horaInicialDia2
            // 
            this.horaInicialDia2.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.horaInicialDia2.Location = new System.Drawing.Point(211, 236);
            this.horaInicialDia2.Name = "horaInicialDia2";
            this.horaInicialDia2.ShowUpDown = true;
            this.horaInicialDia2.Size = new System.Drawing.Size(113, 20);
            this.horaInicialDia2.TabIndex = 64;
            // 
            // Dia2
            // 
            this.Dia2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Dia2.FormattingEnabled = true;
            this.Dia2.Items.AddRange(new object[] {
            "Lunes",
            "Martes",
            "Miercoles",
            "Jueves",
            "Viernes",
            "Sábado",
            "Domingo"});
            this.Dia2.Location = new System.Drawing.Point(84, 236);
            this.Dia2.Name = "Dia2";
            this.Dia2.Size = new System.Drawing.Size(120, 21);
            this.Dia2.TabIndex = 63;
            // 
            // horaFinalDia1
            // 
            this.horaFinalDia1.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.horaFinalDia1.Location = new System.Drawing.Point(329, 211);
            this.horaFinalDia1.Name = "horaFinalDia1";
            this.horaFinalDia1.ShowUpDown = true;
            this.horaFinalDia1.Size = new System.Drawing.Size(113, 20);
            this.horaFinalDia1.TabIndex = 62;
            // 
            // horaInicialDia1
            // 
            this.horaInicialDia1.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.horaInicialDia1.Location = new System.Drawing.Point(210, 210);
            this.horaInicialDia1.Name = "horaInicialDia1";
            this.horaInicialDia1.ShowUpDown = true;
            this.horaInicialDia1.Size = new System.Drawing.Size(113, 20);
            this.horaInicialDia1.TabIndex = 61;
            // 
            // Dia1
            // 
            this.Dia1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Dia1.FormattingEnabled = true;
            this.Dia1.Items.AddRange(new object[] {
            "Lunes",
            "Martes",
            "Miercoles",
            "Jueves",
            "Viernes",
            "Sábado",
            "Domingo"});
            this.Dia1.Location = new System.Drawing.Point(83, 210);
            this.Dia1.Name = "Dia1";
            this.Dia1.Size = new System.Drawing.Size(120, 21);
            this.Dia1.TabIndex = 60;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 217);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 13);
            this.label5.TabIndex = 59;
            this.label5.Text = "Día 1";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(83, 181);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown1.TabIndex = 58;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 188);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 57;
            this.label4.Text = "Capacidad";
            // 
            // txtId
            // 
            this.txtId.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtId.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtId.Location = new System.Drawing.Point(83, 18);
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(79, 20);
            this.txtId.TabIndex = 56;
            this.txtId.Text = "NUEVO";
            this.txtId.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(18, 13);
            this.label3.TabIndex = 55;
            this.label3.Text = "ID";
            // 
            // cbbAsignatura
            // 
            this.cbbAsignatura.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbAsignatura.FormattingEnabled = true;
            this.cbbAsignatura.Location = new System.Drawing.Point(83, 117);
            this.cbbAsignatura.Name = "cbbAsignatura";
            this.cbbAsignatura.Size = new System.Drawing.Size(240, 21);
            this.cbbAsignatura.TabIndex = 54;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 122);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 53;
            this.label2.Text = "Asignatura";
            // 
            // cbbProfesor
            // 
            this.cbbProfesor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbProfesor.FormattingEnabled = true;
            this.cbbProfesor.Location = new System.Drawing.Point(84, 154);
            this.cbbProfesor.Name = "cbbProfesor";
            this.cbbProfesor.Size = new System.Drawing.Size(240, 21);
            this.cbbProfesor.TabIndex = 52;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 162);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 51;
            this.label1.Text = "Profesor";
            // 
            // cbbAula
            // 
            this.cbbAula.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbAula.FormattingEnabled = true;
            this.cbbAula.Location = new System.Drawing.Point(83, 65);
            this.cbbAula.Name = "cbbAula";
            this.cbbAula.Size = new System.Drawing.Size(240, 21);
            this.cbbAula.TabIndex = 50;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 68);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(28, 13);
            this.label9.TabIndex = 49;
            this.label9.Text = "Aula";
            // 
            // cbbCentros
            // 
            this.cbbCentros.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbCentros.FormattingEnabled = true;
            this.cbbCentros.Location = new System.Drawing.Point(83, 41);
            this.cbbCentros.Name = "cbbCentros";
            this.cbbCentros.Size = new System.Drawing.Size(240, 21);
            this.cbbCentros.TabIndex = 67;
            this.cbbCentros.SelectedValueChanged += new System.EventHandler(this.cbbCentros_SelectedValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 44);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 13);
            this.label7.TabIndex = 68;
            this.label7.Text = "Centro";
            // 
            // cbbCarrera
            // 
            this.cbbCarrera.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbCarrera.FormattingEnabled = true;
            this.cbbCarrera.Location = new System.Drawing.Point(83, 93);
            this.cbbCarrera.Name = "cbbCarrera";
            this.cbbCarrera.Size = new System.Drawing.Size(240, 21);
            this.cbbCarrera.TabIndex = 69;
            this.cbbCarrera.SelectedValueChanged += new System.EventHandler(this.cbbCarrera_SelectedValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 96);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 13);
            this.label8.TabIndex = 70;
            this.label8.Text = "Carrera";
            // 
            // frmRegistroSecciones
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(477, 324);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnGuardar);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmRegistroSecciones";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Registro de Secciones";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbbAula;
        private System.Windows.Forms.DateTimePicker horaFinalDia2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker horaInicialDia2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox Dia2;
        private System.Windows.Forms.ComboBox cbbProfesor;
        private System.Windows.Forms.DateTimePicker horaFinalDia1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker horaInicialDia1;
        private System.Windows.Forms.ComboBox cbbAsignatura;
        private System.Windows.Forms.ComboBox Dia1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label txtId;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cbbCentros;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cbbCarrera;
    }
}