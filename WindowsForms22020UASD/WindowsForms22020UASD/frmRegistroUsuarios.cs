﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using INF518Core.Clases;
using INF518Core.Utilitarios;
using INF518Core;

namespace WindowsForms22020UASD
{
    public partial class frmRegistroUsuarios : Form
    {
        Mantenimiento mant;
        Usuario user;
        Usuario _usuario;
        public frmRegistroUsuarios(int Id, Usuario usuario)
        {
            InitializeComponent();
            _usuario = usuario;
            mant = new Mantenimiento(_usuario);
            mant.FillPermisosTreeView(tvPermisos); //Llena el Treview con los permisos.
            CargarTipoUsuario();
            user = new Usuario();
            if(Id>0)
            {
                txtId.Text = Id.ToString();
                user = mant.GetUsuarioInfo(Id);
                txtPassword.Enabled = false; //desactiva los campos de contrasena
                txtConfirmarPassword.Enabled = false; //desactiva los campos de contrasena
                UpdateForm();
            }
            usuario.Id = Id;
        }

        void UpdateForm()
        {
            txtNombre.Text = user.Nombre;
            txtUsuario.Text = user.Username;
            cbTipoEstudiante.SelectedValue = user.IdTipoUsuario;
            mant.UpdateTreeViewPermisos(tvPermisos.Nodes, user.Permisos); //Actualiza los permisos serpardos por ;100; en el treeview.
        }
        void CargarTipoUsuario()
        {
            cbTipoEstudiante.DataSource = mant.GetListadoTipoUsuario(false);
            cbTipoEstudiante.ValueMember = "Id";
            cbTipoEstudiante.DisplayMember = "Nombre";
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if(EsValido())
            {
                UpdateClass();
                mant.GuardarUsuario(user);
                if (mant.Respuesta.ID == 0)
                {
                    MessageBox.Show(
                        "Datos guardados correctamente.",
                        "Información",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                    DialogResult = DialogResult.OK; //Permite saber en la ventana que lo invoca que todo está bien.
                    Close();
                }
                else
                {
                    MessageBox.Show("Se produjo un error.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }
        void UpdateClass()
        {
            user.Nombre = txtNombre.Text;
            user.Password = CryptorEngine.ComputeHash(txtPassword.Text, "SHA512", null); //funcion que genera el hash con algoritmo SHA512
            user.Username = txtUsuario.Text;
            user.Permisos =  ";" + tvPermisos.Nodes.CheckTreeViewCheckedItems(); //agregarle el ; antes de cualquier permiso para que lo delimite.
            user.IdTipoUsuario = Convert.ToInt32(cbTipoEstudiante.SelectedValue);
        }
        bool EsValido()
        {
            if (!txtUsuario.Validar(true))
                return false;
            if (!txtNombre.Validar(true))
                return false;

            if (user.Id == 0) //Para que solo valide si es un usuario nuevo.
            {
                if (!txtPassword.Validar(true))
                    return false;
                if (!txtConfirmarPassword.Validar(true))
                    return false;

                if (!txtPassword.Text.Equals(txtConfirmarPassword.Text))
                {
                    FormsUtils.ShowWarningMessage("Las contrseñas no son iguales.");
                    return false;
                }
            }


            return true;
        }   
    }
}
