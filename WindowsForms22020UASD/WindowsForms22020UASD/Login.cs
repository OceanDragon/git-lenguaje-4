﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using INF518Core;
using INF518Core.Clases;
using INF518Core.Utilitarios;

namespace WindowsForms22020UASD
{
    public partial class Login : Form
    {
        Mantenimiento mant;
        public Login()
        {
            InitializeComponent();
            mant = new Mantenimiento(null);
            Usuario = new Usuario();
        }

        private void Login_Load(object sender, EventArgs e)
        {

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            
            if (!txtUsuario.Validar(true)) //Verifica si el usuario está en blanco
                return;
            if (!txtPassword.Validar(true)) //Verifica si el password esta en blanco.
                return;

            //Verifica si digitaron el usuario supervisor con la clave incorrecta.
            if (txtUsuario.Text.ToUpper().Equals("supervisor".ToUpper()) && !txtPassword.Text.Equals("noquiero"))
            {
                FormsUtils.ShowErrorMessage("Usuario/Contraseña Inválidos");
                return;
            }

            ///Esto es para entrar con este usuario para funciones administrativas
            /// Se deberia de generar un hash por fuera para hacer la comparacion en esta parte.
            if (txtUsuario.Text.ToUpper().Equals("supervisor".ToUpper()) && txtPassword.Text.Equals("noquiero"))
            {
                Usuario = new Usuario
                {
                    Id = 0,
                    Nombre = "Supervisor",
                    Permisos = "*",  // El * tiene permiso a todo.
                    Username = "supervisor"
                };
                DialogResult = DialogResult.OK; //retorna ok
                Close(); //Cierra el formulario
                return; // para que no siga evaluando despues de esta linea.
            }

          

            Usuario user = mant.GetUsuarioInfo(txtUsuario.Text); //busca el usuario e la base de datos
            if(user.Id==0) //si es nulo no existe.
            {
                FormsUtils.ShowErrorMessage("Usuario/Contraseña Inválidos");
                return;
            }

            //Verifica si es otro usuario que no es supervisor...
            if (!txtUsuario.Text.ToUpper().Equals("supervisor"))
            {
                if (!CryptorEngine.VerifyHash(txtPassword.Text, "SHA512", user.Password))
                {
                    FormsUtils.ShowErrorMessage("Usuario/Contraseña Inválidos");
                    return;
                }
                Usuario  = mant.GetUsuarioInfo(user.Id);  //se obtienen todos los datos del usuario que esta validado.
                Usuario.Id = user.Id;
                TipoUsuario tipo = mant.GetTipoUsuarioInfo(Usuario.IdTipoUsuario);
                Usuario.Permisos += tipo.Permisos;
            }


            DialogResult = DialogResult.OK;
            Close();
        }

        public Usuario Usuario { get; set; }
    }
}
