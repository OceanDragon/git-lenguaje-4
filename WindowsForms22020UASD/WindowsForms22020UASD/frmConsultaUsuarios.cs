﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using INF518Core;
using INF518Core.Clases;
using Microsoft.Reporting.WinForms;

namespace WindowsForms22020UASD
{
    public partial class frmConsultaUsuarios : Form
    {
        Mantenimiento mant;
        DataTable dt;
        private readonly Usuario _usuario;
        public frmConsultaUsuarios(Usuario usuario)
        {
            InitializeComponent();
            _usuario = usuario;
            mant = new Mantenimiento(_usuario);
            dt = new DataTable();
            CargarTipoUsuario();
            NewID = 0;
        }
        public int NewID { get; set; }

        void CargarTipoUsuario()
        {
            cbTipoUsuario.DataSource = mant.GetListadoTipoUsuario(true);
            cbTipoUsuario.ValueMember = "Id";
            cbTipoUsuario.DisplayMember = "Nombre";
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            frmRegistroUsuarios frm = new frmRegistroUsuarios(0, _usuario);
            frm.StartPosition = FormStartPosition.CenterScreen;
            if(frm.ShowDialog()==DialogResult.OK)
            {

            }
            frm.Dispose();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            dt = mant.GetListadoUsuarios(ConstruirCriterio());
            //No permite que las columnas se carguen directo del query, se hará con las columnas que se carga explícitamente.
            this.dgvListadoUsuarios.AutoGenerateColumns = false;
            this.dgvListadoUsuarios.DataSource = dt; //asigna el datatatble al datagridview
            if (dt != null && dt.Rows.Count == 0)
            {
                this.dgvListadoUsuarios.DataSource = null;
            }
            ActualizarTotal();
        }
        void ActualizarTotal()
        {
            NewID = 0; //Limpia el Id para futuras busquedas
            this.lblTotal.Text = dt.DefaultView.Count.ToString();
            if (dgvListadoUsuarios.DataSource == null)
                this.lblTotal.Text = string.Empty;
        }
        string ConstruirCriterio()
        {
            StringBuilder criterio = new StringBuilder();
            if (NewID == 0)
            {
                criterio.Append(" u.Id>0 ");
            }
            else
            {
                criterio.AppendFormat(" u.Id={0}", NewID);
            }

            if(Convert.ToInt32(cbTipoUsuario.SelectedValue)>0)
            {
                criterio.AppendFormat(" AND u.IdTipoUsuario={0} ", cbTipoUsuario.SelectedValue);
            }

           if (txtNombre.Text.Trim().Length > 0)
                criterio.AppendFormat(" AND u.Nombre Like '%{0}%' ", txtNombre.Text);

            return criterio.ToString();
        }

        private void txtNombre_TextChanged(object sender, EventArgs e)
        {
            ///Para filtrar solo los registros del datatable que ya está cargado
            if (dt != null && dt.Rows.Count > 0)
            {
                this.dt.DefaultView.RowFilter = ConstruirCriterio();
                ActualizarTotal();
            }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            //Verifica si el datagridview tiene filas
            if (dgvListadoUsuarios.Rows.Count > 0)
            {
                int Id = 0;
                //Esta linea consigue el Id del datagridview para enviarlo al formulario.
                int.TryParse(dgvListadoUsuarios.SelectedRows[0].Cells["colId"].Value.ToString(), out Id);
                if (Id > 0) //Solo carga el formulario si el Id es mayor que 0
                {

                    frmRegistroUsuarios frm = new frmRegistroUsuarios(Id, _usuario);
                    frm.StartPosition = FormStartPosition.CenterScreen;
                    if (frm.ShowDialog() == DialogResult.OK)
                    {
                        NewID = Id; //Asigna NewID, el Id para modificar.
                        btnBuscar_Click(sender, e);
                    }
                    frm.Dispose();
                }
            }
        }

        private void toolStripBuscar_Click(object sender, EventArgs e)
        {
            btnBuscar_Click(sender, e);
        }

        private void toolStripNuevo_Click(object sender, EventArgs e)
        {
            btnNuevo_Click(sender, e);
        }

        private void toolStripModificar_Click(object sender, EventArgs e)
        {
            btnModificar_Click(sender, e);
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            frmReportViewer frm = new frmReportViewer();
            ReportParameter[] paramCollection = new ReportParameter[4];

            //Estos datos deberian venir de la BD o de un archivo json o xml
            paramCollection[0] = new ReportParameter("NombreCompania", "UASD");
            paramCollection[1] = new ReportParameter("RNCCompania", "1234123432");
            paramCollection[2] = new ReportParameter("Direccion", "Salida a Nagua");
            paramCollection[3] = new ReportParameter("Telefono", "809.499.343");


            frm.reportViewer.Reset();

            frm.reportViewer.LocalReport.DataSources.Add(new ReportDataSource("dsConsultaUsuarios", dt));
            
            //Indica el nombre del reporte
            frm.reportViewer.LocalReport.ReportEmbeddedResource = "WindowsForms22020UASD.Reportes.rptUsuarios.rdlc";
           
            //Pasa la coleccion de parametros
            frm.reportViewer.LocalReport.SetParameters(paramCollection);

            frm.reportViewer.RefreshReport();
            frm.ShowDialog();
            frm.Dispose();
        }
    }
}
