﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using INF518Core;
using INF518Core.Clases;

namespace WindowsForms22020UASD
{
    public partial class frmConsultaProfesor : Form
    {
        Mantenimiento mant;
        private readonly Usuario _usuario;
        public frmConsultaProfesor(Usuario usuario)
        {
            InitializeComponent();
            _usuario = usuario;
            mant = new Mantenimiento(_usuario);
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            frmRegistroProfesor frm = new frmRegistroProfesor(0, _usuario); //registro nuevo, cuando es 0.
            frm.StartPosition = FormStartPosition.CenterScreen;
            if(frm.ShowDialog()==DialogResult.OK)
            {
                //TODO: Pendiente
            }
            frm.Dispose();
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            //Verifica si el datagridview tiene filas
            if (dgvListadoProfesores.Rows.Count > 0)
            {
                int Id = 0;
                //Esta linea consigue el Id del datagridview para enviarlo al formulario.
                int.TryParse(dgvListadoProfesores.SelectedRows[0].Cells["colId"].Value.ToString(), out Id);
                if (Id > 0) //Solo carga el formulario si el Id es mayor que 0
                {
                    frmRegistroProfesor frm = new frmRegistroProfesor(Id, _usuario);
                    frm.StartPosition = FormStartPosition.CenterScreen;
                    if (frm.ShowDialog() == DialogResult.OK)
                    {

                    }
                    frm.Dispose();
                }
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            this.dgvListadoProfesores.AutoGenerateColumns = false;
            this.dgvListadoProfesores.DataSource = mant.GetListadoProfesoresSP(0);
        }
    }
}
