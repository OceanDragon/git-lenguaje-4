﻿namespace WindowsForms22020UASD
{
    partial class frmEncriptar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textoPlano = new System.Windows.Forms.TextBox();
            this.textoEncriptado = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.Descifrar = new System.Windows.Forms.Button();
            this.textoCifrado2 = new System.Windows.Forms.TextBox();
            this.textoPlano2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textoPlano3 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textoEnciptado3 = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.Algoritmo1 = new System.Windows.Forms.ComboBox();
            this.Algoritmo = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.button3 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.textPlanoHash = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // textoPlano
            // 
            this.textoPlano.Location = new System.Drawing.Point(5, 121);
            this.textoPlano.Name = "textoPlano";
            this.textoPlano.Size = new System.Drawing.Size(258, 20);
            this.textoPlano.TabIndex = 0;
            // 
            // textoEncriptado
            // 
            this.textoEncriptado.Location = new System.Drawing.Point(443, 121);
            this.textoEncriptado.Name = "textoEncriptado";
            this.textoEncriptado.Size = new System.Drawing.Size(346, 20);
            this.textoEncriptado.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 106);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Texto Plano";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(362, 118);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "Encriptar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Descifrar
            // 
            this.Descifrar.Location = new System.Drawing.Point(276, 67);
            this.Descifrar.Name = "Descifrar";
            this.Descifrar.Size = new System.Drawing.Size(75, 23);
            this.Descifrar.TabIndex = 4;
            this.Descifrar.Text = "Desencriptar";
            this.Descifrar.UseVisualStyleBackColor = true;
            this.Descifrar.Click += new System.EventHandler(this.Descifrar_Click);
            // 
            // textoCifrado2
            // 
            this.textoCifrado2.Location = new System.Drawing.Point(9, 70);
            this.textoCifrado2.Name = "textoCifrado2";
            this.textoCifrado2.Size = new System.Drawing.Size(261, 20);
            this.textoCifrado2.TabIndex = 5;
            // 
            // textoPlano2
            // 
            this.textoPlano2.Location = new System.Drawing.Point(357, 69);
            this.textoPlano2.Name = "textoPlano2";
            this.textoPlano2.Size = new System.Drawing.Size(432, 20);
            this.textoPlano2.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(357, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Texto Plano";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(443, 106);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Texto Cifrado";
            // 
            // textoPlano3
            // 
            this.textoPlano3.Location = new System.Drawing.Point(9, 26);
            this.textoPlano3.Name = "textoPlano3";
            this.textoPlano3.Size = new System.Drawing.Size(261, 20);
            this.textoPlano3.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Texto Plano";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(357, 13);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Texto Cifrado";
            // 
            // textoEnciptado3
            // 
            this.textoEnciptado3.Location = new System.Drawing.Point(357, 29);
            this.textoEnciptado3.Name = "textoEnciptado3";
            this.textoEnciptado3.Size = new System.Drawing.Size(432, 20);
            this.textoEnciptado3.TabIndex = 13;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(276, 26);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 15;
            this.button2.Text = "Encriptar";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Algoritmo1
            // 
            this.Algoritmo1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Algoritmo1.FormattingEnabled = true;
            this.Algoritmo1.Items.AddRange(new object[] {
            "MD5",
            "SHA1",
            "SHA256",
            "SHA384",
            "SHA512"});
            this.Algoritmo1.Location = new System.Drawing.Point(269, 120);
            this.Algoritmo1.Name = "Algoritmo1";
            this.Algoritmo1.Size = new System.Drawing.Size(87, 21);
            this.Algoritmo1.TabIndex = 16;
            // 
            // Algoritmo
            // 
            this.Algoritmo.AutoSize = true;
            this.Algoritmo.Location = new System.Drawing.Point(266, 106);
            this.Algoritmo.Name = "Algoritmo";
            this.Algoritmo.Size = new System.Drawing.Size(50, 13);
            this.Algoritmo.TabIndex = 17;
            this.Algoritmo.Text = "Algoritmo";
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "MD5",
            "SHA1",
            "SHA256",
            "SHA384",
            "SHA512"});
            this.comboBox1.Location = new System.Drawing.Point(271, 157);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(87, 21);
            this.comboBox1.TabIndex = 23;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(364, 158);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 21;
            this.button3.Text = "Verificar";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 143);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(64, 13);
            this.label8.TabIndex = 20;
            this.label8.Text = "Texto Plano";
            // 
            // textPlanoHash
            // 
            this.textPlanoHash.Location = new System.Drawing.Point(7, 158);
            this.textPlanoHash.Name = "textPlanoHash";
            this.textPlanoHash.Size = new System.Drawing.Size(258, 20);
            this.textPlanoHash.TabIndex = 18;
            // 
            // frmEncriptar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(801, 203);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.textPlanoHash);
            this.Controls.Add(this.Algoritmo);
            this.Controls.Add(this.Algoritmo1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textoEnciptado3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textoPlano3);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textoPlano2);
            this.Controls.Add(this.textoCifrado2);
            this.Controls.Add(this.Descifrar);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textoEncriptado);
            this.Controls.Add(this.textoPlano);
            this.Name = "frmEncriptar";
            this.Text = "Encriptar / Desincriptar";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textoPlano;
        private System.Windows.Forms.TextBox textoEncriptado;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button Descifrar;
        private System.Windows.Forms.TextBox textoCifrado2;
        private System.Windows.Forms.TextBox textoPlano2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textoPlano3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textoEnciptado3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ComboBox Algoritmo1;
        private System.Windows.Forms.Label Algoritmo;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textPlanoHash;
    }
}