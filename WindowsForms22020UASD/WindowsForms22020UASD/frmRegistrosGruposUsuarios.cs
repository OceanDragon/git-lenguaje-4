﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using INF518Core;
using INF518Core.Clases;
using INF518Core.Utilitarios;

namespace WindowsForms22020UASD
{
    public partial class frmRegistrosGruposUsuarios : Form
    {
        TipoUsuario tUsuario;
        Mantenimiento mant;
        private readonly Usuario _usuario;
        public frmRegistrosGruposUsuarios(int Id, Usuario usuario)
        {
            InitializeComponent();
            tUsuario = new TipoUsuario();
            _usuario = usuario;
            mant = new Mantenimiento(_usuario);
            mant.FillPermisosTreeView(tvPermisos); //Llena el Treview con los permisos.
            if (Id > 0)
            {
                txtId.Text = Id.ToString();
                tUsuario = mant.GetTipoUsuarioInfo(Id);
                UpdateForm();
            }
            tUsuario.Id = Id;
        }
        void UpdateForm()
        {
            txtNombre.Text = tUsuario.Nombre;
            mant.UpdateTreeViewPermisos(tvPermisos.Nodes, tUsuario.Permisos); //Actualiza los permisos serpardos por ;100; en el treeview.
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (EsValido())
            {
                UpdateClass();
                mant.GuardarTipoUsuario(tUsuario);
                if (mant.Respuesta.ID == 0)
                {
                    MessageBox.Show(
                        "Datos guardados correctamente.",
                        "Información",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                    DialogResult = DialogResult.OK; //Permite saber en la ventana que lo invoca que todo está bien.
                    Close();
                }
                else
                {
                    MessageBox.Show("Se produjo un error.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }
        bool EsValido()
        {
            if (!txtNombre.Validar(true))
                return false;
            if(tvPermisos.Nodes.CheckTreeViewCheckedItems().Trim().Length==0)
            {
                INF518Core.Utilitarios.FormsUtils.ShowWarningMessage("Debe elegir al menos un permiso.");
                return false;
            }

            return true;
        }
        void UpdateClass()
        {
            tUsuario.Nombre = txtNombre.Text;
            tUsuario.Permisos = ";" + tvPermisos.Nodes.CheckTreeViewCheckedItems(); //agregarle el ; antes de cualquier permiso para que lo delimite.
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
