﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using INF518Core.Clases;
using INF518Core;

namespace WindowsForms22020UASD
{
    public partial class frmRegistroEstudiante : Form
    {
        Estudiante estudiante;
        Mantenimiento mant;
        private readonly Usuario _usuario;
        public frmRegistroEstudiante(int Id, Usuario usuario)
        {
            InitializeComponent();
            _usuario = usuario;
            estudiante = new Estudiante();
            estudiante.Id = Id;
            mant = new Mantenimiento(_usuario);
            
            ///Carga el estado civil
            CargarEstadoCivil();
            if(cbbEstadoCivil.Items.Count>0)
                this.cbbEstadoCivil.SelectedIndex = 0; //selecciona la primera opción
 
            //Cuando el ID==0 se asume que es un reigstro nuevo, cuando es mayor se asume que se hará un UPDATE
            if(Id>0)
            {
                this.txtId.Text = Id.ToString();
                estudiante = mant.GetEstudianteInfo(Id); //Busca los datos del estudiante y devuelve un objeto estudiante
                estudiante.Id = Id;

                UpdateForm(); //Carga los datos de la clase estudiante en el formulario.
            }
        }
        public int NewID
        {
            get
            {
                return mant.Respuesta.NewID;
            }        
                    
        }
        void UpdateForm() //Pasa los datos del objeto al formulario
        {
            this.txtNombre.Text = estudiante.Nombre;
            this.txtApellido.Text = estudiante.Apellido;
            this.mtbCedula.Text = estudiante.Cedula;
            this.txtOcupacion.Text = estudiante.Ocupacion;
            this.cbbEstadoCivil.SelectedValue = estudiante.EstadoCivil;
            this.dtpFechaNacimiento.Value = estudiante.FechaNacimiento;
            if (estudiante.Sexo.Equals('M'))
                rbMasculino.Checked = true;
            if (estudiante.Sexo.Equals('F'))
                rbFemenino.Checked = true;
        }

        void CargarEstadoCivil()
        {
            cbbEstadoCivil.DataSource = mant.GetListadoEstadoCivil();
            cbbEstadoCivil.ValueMember = "Nombre";
            cbbEstadoCivil.DisplayMember = "Nombre";
        }
        private void btnCargar_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Imagénes (jpg, png) | *.jpg; *.png ";
            if (openFileDialog1.ShowDialog()==DialogResult.OK)
            {
                pictureBox1.ImageLocation = openFileDialog1.FileName;
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            this.pictureBox1.ImageLocation = string.Empty;
        }

 



        private void btnGuardar_Click(object sender, EventArgs e)
        {
            errorProvider1.Clear();
            if(ValidarCampos())
            {
                UpdateClase(); //Copia los miembros del formulario a la clase que se le pasa al método como parámetro.
                mant.GuardarEstudianteSP(estudiante);
                if(mant.Respuesta.ID==0)
                {
                    MessageBox.Show(
                        "Datos guardados correctamente.", 
                        "Información",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                    DialogResult = DialogResult.OK; //Permite saber en la ventana que lo invoca que todo está bien.
                    Close();
                }
                else
                {
                    MessageBox.Show("Se produjo un error.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
           
        }
        /// <summary>
        /// Copia los datos del fromulario en la clase
        /// para guardar la info en la BD.
        /// </summary>
        void UpdateClase()
        {
            estudiante.Nombre = this.txtNombre.Text;
            estudiante.Apellido = txtApellido.Text;
            estudiante.Cedula = mtbCedula.Text;
            estudiante.Ocupacion = txtOcupacion.Text;
            if (rbMasculino.Checked)
                estudiante.Sexo = 'M';
            if (rbFemenino.Checked)
                estudiante.Sexo = 'F';
            estudiante.EstadoCivil = cbbEstadoCivil.Text.ToString();
            estudiante.FechaNacimiento = dtpFechaNacimiento.Value.Date;
        }
        bool ValidarCampos()
        {
            if (txtNombre.Text.Trim().Length == 0)
            {
                errorProvider1.SetError(txtNombre, "El nombre no puede estar en blanco.");
                return false;
            }
            if (txtApellido.Text.Trim().Length == 0)
            {
                errorProvider1.SetError(txtApellido, "El apellido no puede estar en blanco.");
                return false;
            }
            return true;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
