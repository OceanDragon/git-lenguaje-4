﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Security.Cryptography;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using INF518Core.Utilitarios;

namespace WindowsForms22020UASD
{
    public partial class frmEncriptar : Form
    {
        public frmEncriptar()
        {
            InitializeComponent();
            this.Algoritmo1.SelectedIndex = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string valor = this.Algoritmo1.SelectedItem.ToString();
            textoEncriptado.Text = CryptorEngine.ComputeHash(textoPlano.Text,valor,null);
        }

        private void Descifrar_Click(object sender, EventArgs e)
        {
            textoPlano2.Text = CryptorEngine.Decrypt(textoCifrado2.Text, true);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textoEnciptado3.Text = CryptorEngine.Encrypt(textoPlano3.Text, true);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string valor = this.Algoritmo1.SelectedItem.ToString();
            if (CryptorEngine.VerifyHash(this.textPlanoHash.Text, valor, this.textoEncriptado.Text))
            {
                MessageBox.Show("OK");
           }
        }
        /// <summary>
        /// Genera un hash de una sola via para la data que se esta cifrando.
        /// </summary>
        /// <param name="PlainText"></param>
        /// <returns></returns>
      
    }
}
