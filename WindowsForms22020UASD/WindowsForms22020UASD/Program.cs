﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using INF518Core;
using INF518Core.Clases;


namespace WindowsForms22020UASD
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Login frm = new Login();
            frm.StartPosition = FormStartPosition.CenterScreen;
            Application.Run(frm);

            if (frm.DialogResult == DialogResult.OK)
            {
                frm.Dispose();
                Application.Run(new MainForm(frm.Usuario));
            }
        }
    }
}
