﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using INF518Core.Utilitarios;
using INF518Core.Clases;
using INF518Core;

namespace WindowsForms22020UASD
{
    public partial class frmCambiarContrasena : Form
    {

        private readonly Mantenimiento mant;
        private readonly Usuario _usuario;
        public frmCambiarContrasena(Usuario usuario)
        {
            InitializeComponent();
            _usuario = usuario;
            mant = new Mantenimiento(_usuario);
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if(EsValido())
            {
                mant.CambiarContrasena(_usuario.Id, CryptorEngine.ComputeHash(txtPassword.Text, "SHA512", null));
                if (mant.Respuesta.ID == 0)
                {
                    MessageBox.Show(
                        "Contraseña cambiada correctamente.",
                        "Información",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                    DialogResult = DialogResult.OK; //Permite saber en la ventana que lo invoca que todo está bien.
                    Close();
                }
                else
                {
                    MessageBox.Show("Se produjo un error.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }
        bool EsValido()
        {
            if (!txtPasswordActual.Validar(true))
                return false;

            if (!txtPassword.Validar(true))
                return false;
            if (!txtConfirmarPassword.Validar(true))
                return false;

            if (!txtPassword.Text.Equals(txtConfirmarPassword.Text))
            {
                FormsUtils.ShowWarningMessage("Las contrseñas no son iguales.");
                return false;
            }

            if(!CryptorEngine.VerifyHash(txtPasswordActual.Text, "SHA512", _usuario.Password)) //crea el hash antes de enviarlo por la red
            {
                FormsUtils.ShowErrorMessage("Contraseña actual incorrecta.");
                return false;
            }


             return true;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
