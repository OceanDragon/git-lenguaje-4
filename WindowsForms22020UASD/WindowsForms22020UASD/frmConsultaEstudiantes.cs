﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using INF518Core.Clases;
using INF518Core;

namespace WindowsForms22020UASD
{
    public partial class frmConsultaEstudiantes : Form
    {
        Mantenimiento mant; //El objeto que tiene todas las interacciones a la base de datos.
        DataTable dt; // Este obj debe ser con alcance de clase para poderlo filtrar localmente.
        private readonly Usuario _usuario;
        public frmConsultaEstudiantes(Usuario usuario)
        {
            InitializeComponent();
            _usuario = usuario;
            mant = new Mantenimiento(_usuario); //Instanciando el objeto
            NewID = 0;
        }

        public int NewID { get; set; }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            frmRegistroEstudiante frm = new frmRegistroEstudiante(0, _usuario); //se pasa 0 para indicar que es nuevo.
            frm.StartPosition = FormStartPosition.CenterScreen;
            if(frm.ShowDialog()==DialogResult.OK)
            {
                NewID = frm.NewID; //Recibe el nuevo ID o el de modificar del formulario de estudiantes
                btnBuscar_Click(sender, e);
            }
            frm.Dispose();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {   dt = mant.GetListadoEstudiantes(ConstruirCriterio());
            //No permite que las columnas se carguen directo del query, se hará con las columnas que se carga explícitamente.
            this.dgvListadoEstudiantes.AutoGenerateColumns = false;
            this.dgvListadoEstudiantes.DataSource = dt; //asigna el datatatble al datagridview
            if(dt!=null && dt.Rows.Count==0)
            {
                this.dgvListadoEstudiantes.DataSource = null;
            }
            ActualizarTotal();  
            
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            //Verifica si el datagridview tiene filas
            if(dgvListadoEstudiantes.Rows.Count>0)
            {
                int Id = 0;
                //Esta linea consigue el Id del datagridview para enviarlo al formulario.
                int.TryParse(dgvListadoEstudiantes.SelectedRows[0].Cells["colId"].Value.ToString(), out Id); 
                if(Id>0) //Solo carga el formulario si el Id es mayor que 0
                {
                   
                    frmRegistroEstudiante frm = new frmRegistroEstudiante(Id, _usuario);
                    frm.StartPosition = FormStartPosition.CenterScreen;
                    if(frm.ShowDialog()==DialogResult.OK)
                    {
                        NewID = Id; //Asigna NewID, el Id para modificar.
                        btnBuscar_Click(sender, e);
                    }
                    frm.Dispose();
                }
            }
        }

        private void txtNombre_TextChanged(object sender, EventArgs e)
        {
            ///Para filtrar solo los registros del datatable que ya está cargado
            if(dt!=null && dt.Rows.Count>0)
            {
                this.dt.DefaultView.RowFilter = ConstruirCriterio();
                ActualizarTotal();
            }
        }

        string ConstruirCriterio()
        {
            StringBuilder criterio = new StringBuilder();
            if (NewID==0)
            {
                criterio.Append(" IdEstudiante>0 ");
            }
            else
            {
                criterio.AppendFormat(" IdEstudiante={0}", NewID);
            }

            ///Este codigo verifica los literales de la cedula para buscar por cualquier digito de la cedula
            int cont = 0;
            foreach(char valor in mtbCedula.Text.ToCharArray())
            {
                if (Char.IsDigit(valor))
                    cont++;
                if (cont == 3 || cont == 12)
                    cont++;
            }

            

            if (txtNombre.Text.Trim().Length > 0)
                criterio.AppendFormat(" AND Nombre Like '%{0}%' ", txtNombre.Text);
            if (mtbCedula.Text.Trim().Length>0)
                criterio.AppendFormat(" AND Cedula LIKE '{0}%'", mtbCedula.Text.Substring(0,cont));

            return criterio.ToString();
        }


        /// <summary>
        /// Verifica los datos necesarios y actualiza el label que tiene el total de registros recuperados.
        /// </summary>
        void ActualizarTotal()
        {
            NewID = 0; //Limpia el Id para futuras busquedas
            this.lblTotal.Text = dt.DefaultView.Count.ToString();
            if(dgvListadoEstudiantes.DataSource==null)
                this.lblTotal.Text = string.Empty;
        }
    }
}
