﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INF518Core.Utilitarios;
using INF518Core;
using INF518Core.Clases;

namespace WindowsForms22020UASD
{
    public partial class frmControlesListas : Form
    {
        MenuPrincipal _menu;
        Mantenimiento mant;
        private readonly Usuario _usuario;
        public frmControlesListas(MenuStrip menu, Usuario usuario)
        {
            Application.EnableVisualStyles();
            InitializeComponent();
            _usuario = usuario;
            _menu = new MenuPrincipal(menu);
            mant = new Mantenimiento(_usuario); ///Recibe el usuario conectado al sistema como parámetro.
            mant.FillPermisosListView(this.listView1);
            mant.FillPermisosGroupListListView(this.listView2);
            //Aquí esta repetido el código que coteja todos los checkbox del listview.. Hacer uno solo como tarea.
        }

        
        /// <summary>
        /// Para recorrer el listview y luego introducirlo en un string 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void VerificaPermisos_Click(object sender, EventArgs e)
        {

            txtListViewPermisos.Text = listView1.GetListViewCheckedItems();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //select all
            foreach (ListViewItem item in listView1.Items)
            {
                item.Checked = true;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //unselect all
            foreach (ListViewItem item in listView1.Items)
            {
                item.Checked = false;
            }
        }
        private void VerificaPermisos2_Click(object sender, EventArgs e)
        {

            this.txtPermisos2.Text = listView2.GetListViewCheckedItems();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in listView2.Items)
            {
                item.Checked = true;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in listView2.Items)
            {
                item.Checked = false;
            }
        }

        private void UpdateMainMenu_Click(object sender, EventArgs e)
        {
            if (this.txtListViewPermisos.Text.Trim().Length == 0)
            {
                MessageBox.Show("Primero debe elegir un permiso y luego presionar Verificar Permiso.",
                                "Información",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
                return;
            }
            _menu.UpdateMenuItems(this.txtListViewPermisos.Text);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (txtPermisos2.Text.Trim().Length == 0)
            {
                MessageBox.Show("Primero debe elegir un permiso y luego presionar Verificar Permiso.",
                              "Información",
                              MessageBoxButtons.OK,
                              MessageBoxIcon.Information);
                return;
            }
            _menu.UpdateMenuItems(this.txtPermisos2.Text);
        }
       

          /// <summary>
        /// Actualiza el treeview recursivo
        /// </summary>
        /// <param name="nodes"></param>
        void UpdateTreeView(TreeNodeCollection nodes, bool Checked)
        {
            foreach (TreeNode node in nodes)
            {
                node.Checked = Checked;
                if (node.Nodes.Count > 0)
                    UpdateTreeView(node.Nodes, Checked);
            }
        }

    }
}
