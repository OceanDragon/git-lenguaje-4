﻿namespace WindowsForms22020UASD
{ 
partial class frmControlesListas
{
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.txtListViewPermisos = new System.Windows.Forms.TextBox();
            this.VerificaPermisos = new System.Windows.Forms.Button();
            this.listView2 = new System.Windows.Forms.ListView();
            this.colPermisos = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.VerificaPermisos2 = new System.Windows.Forms.Button();
            this.txtPermisos2 = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.UpdateMainMenu = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.listView1.CheckBoxes = true;
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(12, 12);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(311, 185);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Permisos";
            this.columnHeader1.Width = 300;
            // 
            // txtListViewPermisos
            // 
            this.txtListViewPermisos.Location = new System.Drawing.Point(329, 53);
            this.txtListViewPermisos.Multiline = true;
            this.txtListViewPermisos.Name = "txtListViewPermisos";
            this.txtListViewPermisos.Size = new System.Drawing.Size(275, 69);
            this.txtListViewPermisos.TabIndex = 1;
            // 
            // VerificaPermisos
            // 
            this.VerificaPermisos.Location = new System.Drawing.Point(329, 12);
            this.VerificaPermisos.Name = "VerificaPermisos";
            this.VerificaPermisos.Size = new System.Drawing.Size(100, 35);
            this.VerificaPermisos.TabIndex = 2;
            this.VerificaPermisos.Text = "Verificar Permisos";
            this.VerificaPermisos.UseVisualStyleBackColor = true;
            this.VerificaPermisos.Click += new System.EventHandler(this.VerificaPermisos_Click);
            // 
            // listView2
            // 
            this.listView2.CheckBoxes = true;
            this.listView2.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colPermisos});
            this.listView2.HideSelection = false;
            this.listView2.Location = new System.Drawing.Point(12, 213);
            this.listView2.MultiSelect = false;
            this.listView2.Name = "listView2";
            this.listView2.Size = new System.Drawing.Size(311, 217);
            this.listView2.TabIndex = 3;
            this.listView2.UseCompatibleStateImageBehavior = false;
            this.listView2.View = System.Windows.Forms.View.Details;
            // 
            // colPermisos
            // 
            this.colPermisos.Text = "Permisos";
            this.colPermisos.Width = 300;
            // 
            // VerificaPermisos2
            // 
            this.VerificaPermisos2.Location = new System.Drawing.Point(329, 215);
            this.VerificaPermisos2.Name = "VerificaPermisos2";
            this.VerificaPermisos2.Size = new System.Drawing.Size(100, 35);
            this.VerificaPermisos2.TabIndex = 5;
            this.VerificaPermisos2.Text = "Verificar Permisos";
            this.VerificaPermisos2.UseVisualStyleBackColor = true;
            this.VerificaPermisos2.Click += new System.EventHandler(this.VerificaPermisos2_Click);
            // 
            // txtPermisos2
            // 
            this.txtPermisos2.Location = new System.Drawing.Point(329, 256);
            this.txtPermisos2.Multiline = true;
            this.txtPermisos2.Name = "txtPermisos2";
            this.txtPermisos2.Size = new System.Drawing.Size(275, 90);
            this.txtPermisos2.TabIndex = 4;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(329, 128);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(100, 28);
            this.button2.TabIndex = 6;
            this.button2.Text = "Selecionar Todo";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(435, 128);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(100, 28);
            this.button3.TabIndex = 7;
            this.button3.Text = "Seleccionar Nada";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(329, 352);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 28);
            this.button1.TabIndex = 8;
            this.button1.Text = "Selecionar Todo";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(435, 352);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(100, 28);
            this.button4.TabIndex = 9;
            this.button4.Text = "Seleccionar Nada";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // UpdateMainMenu
            // 
            this.UpdateMainMenu.Location = new System.Drawing.Point(329, 162);
            this.UpdateMainMenu.Name = "UpdateMainMenu";
            this.UpdateMainMenu.Size = new System.Drawing.Size(275, 35);
            this.UpdateMainMenu.TabIndex = 10;
            this.UpdateMainMenu.Text = "Actualizar Menú Principal";
            this.UpdateMainMenu.UseVisualStyleBackColor = true;
            this.UpdateMainMenu.Click += new System.EventHandler(this.UpdateMainMenu_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(329, 395);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(275, 35);
            this.button5.TabIndex = 11;
            this.button5.Text = "Actualizar Menú Principal";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // frmControlesListas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(619, 438);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.UpdateMainMenu);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.VerificaPermisos2);
            this.Controls.Add(this.txtPermisos2);
            this.Controls.Add(this.listView2);
            this.Controls.Add(this.VerificaPermisos);
            this.Controls.Add(this.txtListViewPermisos);
            this.Controls.Add(this.listView1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmControlesListas";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Ejemplos Carga Permisos";
            this.ResumeLayout(false);
            this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.ListView listView1;
    private System.Windows.Forms.ColumnHeader columnHeader1;
    private System.Windows.Forms.TextBox txtListViewPermisos;
    private System.Windows.Forms.Button VerificaPermisos;
    private System.Windows.Forms.ListView listView2;
    private System.Windows.Forms.Button VerificaPermisos2;
    private System.Windows.Forms.TextBox txtPermisos2;
    private System.Windows.Forms.ColumnHeader colPermisos;
    private System.Windows.Forms.Button button2;
    private System.Windows.Forms.Button button3;
    private System.Windows.Forms.Button button1;
    private System.Windows.Forms.Button button4;
    private System.Windows.Forms.Button UpdateMainMenu;
    private System.Windows.Forms.Button button5;
    //private ucneDataSet2011TableAdapters.tblProductsTableAdapter tblProductsTableAdapter1;
}
}