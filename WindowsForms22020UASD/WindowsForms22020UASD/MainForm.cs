﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using INF518Core.Clases;
using INF518Core.Utilitarios;

namespace WindowsForms22020UASD
{
    public partial class MainForm : Form
    {
        Usuario _usuario;
        MenuPrincipal menu;
        private int hr = 0;
        private int seg = 0;
        private int min = 0;
        public MainForm(Usuario usuario)
        {
            InitializeComponent();
            this._usuario = usuario; // copia el usuario que recibe como parámetro y lo coloca con alcance de clase.
            this.tsNombreUsuario.Text = usuario.Nombre.ToUpper(); //asigna el usuario que se conectó al StatusStrip que está en el fondo.
            menu = new MenuPrincipal(this.MenuMain); //Se instancia la clase MenuPrincipal con el menu del mainform.
            if(!_usuario.Permisos.Equals("*")) //Si es * tiene todos los permisos, no hay restriccciones, no se aplica el Update
                menu.UpdateMenuItems(usuario.Permisos);
            timer1.Start();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            timer1.Stop();
            Application.Exit();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("¿Está seguro que desea salir del sistema?", 
                "Advertencia", MessageBoxButtons.OKCancel, 
                MessageBoxIcon.Question) == DialogResult.Cancel)
            {
                e.Cancel = true;
                return;
            }
        }

        private void mantenimientosToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void estudianteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmConsultaEstudiantes frm = new frmConsultaEstudiantes(_usuario);
            frm.MdiParent = this;
            frm.StartPosition = FormStartPosition.CenterScreen;
            frm.Show();
        }

        private void dialogosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            btnFuente frm = new btnFuente(); //crea el objeto
            frm.MdiParent = this; //le dice que no se salga del padre
            frm.StartPosition = FormStartPosition.CenterParent; // lo centraliza
            frm.Show(); //lo muestra

        }

        private void wordpadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmWordpad frm = new frmWordpad();
            frm.StartPosition = FormStartPosition.CenterScreen;
            frm.Show();
        }

        private void otraPruebaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmRegistroEstudiantes frm = new frmRegistroEstudiantes();
            frm.StartPosition = FormStartPosition.CenterScreen;
            // frm.MdiParent = this;
            //frm.Show();

            if (frm.ShowDialog() == DialogResult.OK)
            {
                MessageBox.Show("Presiono OK.");
                MessageBox.Show(frm.txtNombre.Text);
            }
            frm.Dispose(); //libera la memoria
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == this.WindowState)
            {
                notifyIcon1.Visible = true;
                notifyIcon1.ShowBalloonTip(1000);
                this.Hide();
            }
            else if (FormWindowState.Normal == this.WindowState)
            {
                notifyIcon1.Visible = false;
            }
        }

        private void abrirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Maximized;
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
        }

        private void profesorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmConsultaProfesor frm = new frmConsultaProfesor(_usuario);
            frm.MdiParent = this;
            frm.StartPosition = FormStartPosition.CenterScreen;
            frm.Show();
        }

        private void usuariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmConsultaUsuarios frm = new frmConsultaUsuarios(_usuario);
            frm.StartPosition = FormStartPosition.CenterScreen;
            frm.MdiParent = this;
            frm.Show();
        }

        private void cambiarContraseñaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCambiarContrasena frm = new frmCambiarContrasena(_usuario);
            frm.StartPosition = FormStartPosition.CenterScreen;
            frm.MdiParent = this;
            frm.Show();
        }

        private void gruposDeUsuariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmListadoGruposUsuarios frm = new frmListadoGruposUsuarios(_usuario);
            frm.MdiParent = this;
            frm.StartPosition = FormStartPosition.CenterScreen;
            frm.Show();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            StringBuilder strReloj = new StringBuilder();
            strReloj.Append((hr < 10) ? "0" + Convert.ToString(hr) : Convert.ToString(hr));
            strReloj.Append(":");
            strReloj.Append((min < 10) ? "0" + Convert.ToString(min) : Convert.ToString(min));
            strReloj.Append(":");
            strReloj.Append((seg < 10) ? "0" + Convert.ToString(seg) : Convert.ToString(seg));
            this.tsTimer.Text = strReloj.ToString();

            if (seg == 59)
            {
                min++;
                seg = 0;
            }
            if (min == 59)
            {
                min = 0;
                hr++;
            }
            seg++;
        }

        private void encriptarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmEncriptar frm = new frmEncriptar();
            frm.MdiParent = this;
            frm.StartPosition = FormStartPosition.CenterScreen;
            frm.Show();
        }

        private void controlesPermisosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmControlesListas frm = new frmControlesListas(this.MenuMain, _usuario);
            frm.MdiParent = this;
            frm.StartPosition = FormStartPosition.CenterScreen;
            frm.Show();
        }

        private void reportesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmReportViewer frm = new frmReportViewer();
            frm.MdiParent = this;
            frm.StartPosition = FormStartPosition.CenterScreen;
            frm.Show();
        }

        private void seccionesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmRegistroSecciones frm = new frmRegistroSecciones(0, _usuario);
            frm.MdiParent = this;
            frm.StartPosition = FormStartPosition.CenterScreen;
            frm.Show();
        }
    }
}
