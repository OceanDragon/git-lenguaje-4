﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INF518Core.Clases
{
    public class Seccion
    {
        public int IdAsignatura { get; set; }
        public int IdAula { get; set; }
        public int IdProfesor { get; set; }
        public int Dia1 { get; set; }
        public int HoraInicioDia1 { get; set; } //10 - 12 no se pueder colocar una de 11-1 p.m.
        public int HoraFinalDia1 { get; set; }
        public int Dia2 { get; set; }
        public int HoraInicioDia2 { get; set; }
        public int HoraFinalDia2 { get; set; }


    }
}
