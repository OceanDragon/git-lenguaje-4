﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INF518Core.Clases
{
    public class Estudiante
    {
        public int Id { get; set; }

        public string Cedula { set; get; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public string Ocupacion { get; set; }

        public string EstadoCivil { get; set; }

        public char Sexo { get; set; }
    }
}
