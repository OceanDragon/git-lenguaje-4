﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INF518Core.Clases
{
    public class TipoUsuario
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Permisos { get; set; }

        public bool Inactivo { get; set; }
    }
}
