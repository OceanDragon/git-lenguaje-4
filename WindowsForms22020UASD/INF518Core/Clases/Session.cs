﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INF518Core.Clases
{
    public class Session
    {
        public int UserID { get; set; }
        public string Username { get; set; }
        public string Nombre { get; set; }
        public string Password { get; set; }

        public string Permisos { get; set; }
    }
}
