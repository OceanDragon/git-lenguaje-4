﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using INF518Core.Clases;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace INF518Core
{
    public class Mantenimiento : ClaseBase
    {
        private readonly Usuario _usuario;
        public Mantenimiento(Usuario usuario)
        {
            _usuario = usuario;
        }

        #region Operaciones de Estudiantes


        /// <summary>
        /// Devuelve un DataTable con los datos de los estudiantes.
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="filtro"></param>
        /// <returns></returns>
        public DataTable GetListadoEstudiantes(string filtro)
        {
            StringBuilder sql = new StringBuilder("SELECT ");
            sql.Append(" IdEstudiante, Nombre, Apellido, Cedula, Ocupacion, Sexo, FechaNacimiento [Nacimiento] FROM tblEstudiantes ");
            if (filtro.Length > 0)
                sql.AppendFormat(" WHERE {0}", filtro);
            return GetDataTableFromSQL(sql.ToString());
        }


        public bool VerificarSecciones(Seccion seccion)
        {
            //Validar Aula
            Query.AppendFormat("SELECT Id FROM tblSecciones WHERE IdAula={0} AND Dia1={1} AND Hora1={2} AND Dia2={3} AND Hora2={4) ", 
                    seccion.IdAula,
                    seccion.Dia1,
                    seccion.HoraInicioDia1,
                    seccion.Dia2,
                    seccion.HoraInicioDia1
                    );  ///HoraInicialDia1>=seccion.HoraInicialDia1 and HoraFinalDia1<=seccion.HoraFinalDia1

            ///(seccion.HoraInicialDia1>=HoraInicialDia1 and seccion.HoraFinalDia1<HoraInicialDia1)
            ///(seccion.HoraFinalDia1>=HoraInicialDia1 and seccion.HoraFinalDia1<HoraFinalDia1)

            //10-12 HoraInicialDia1 de la dB
            //11-1pm
            //8 a.m. 1 p.m.
            //10 en adelante
            //11 en adelante

            return true;
        }


        public Estudiante GetEstudianteInfo(int Id)
        {
            Estudiante item = new Estudiante();
            Query.Clear(); //Limpia el stringuilder
            Query.AppendFormat("SELECT IdEstudiante, Nombre, Apellido, Cedula, Ocupacion, FechaNacimiento, Sexo, EstadoCivil FROM tblEstudiantes WHERE IdEstudiante={0}", Id);
            try
            {
                Connection.Open();
                Command = new SqlCommand(Query.ToString(), Connection);
                SqlDataReader reader = Command.ExecuteReader(); //Para revisar los datos de la consulta es más fácil un DataReader
                if(reader.HasRows)
                {
                    while(reader.Read())
                    {
                        item.Nombre = reader["Nombre"].ToString();
                        item.Apellido = reader["Apellido"].ToString();
                        item.Ocupacion = reader["Ocupacion"].ToString();
                        item.EstadoCivil = reader["EstadoCivil"].ToString();
                        item.Sexo = Convert.ToChar(reader["Sexo"]);
                        item.Cedula = reader["Cedula"].ToString();
                        item.FechaNacimiento = Convert.ToDateTime(reader["FechaNacimiento"]); // reader.GetDateTime(5)
                    }
                }
            }
            catch(Exception ex)
            {
                Respuesta.ID = 1;
                Respuesta.Descripcion = ex.Message;
            }
            finally
            {
                Connection.Close();
            }

            return item;
        }


        /// <summary>
        /// Permite insertar o actualizar los datos de un estudiante.
        /// </summary>
        /// <param name="item">Un objeto tipo estudiante</param>
        public void GuardarEstudiante(Estudiante item)
        {
            Respuesta = new Respuesta();
            Query.Clear();
            if(item.Id==0) //NUEVO SE USA INSERT
            {
                Query.Append("INSERT INTO tblEstudiantes (Nombre, Apellido, Cedula, Sexo, FechaNacimiento, Ocupacion, EstadoCivil, Creado, CreadoPorID, Modificado, ModificadoPorID) ");
                Query.AppendFormat(" VALUES ('{0}','{1}','{2}','{3}','{4}','{5}','{6}', '{7}',{8},'{9}',{10} )",
                    item.Nombre,
                    item.Apellido,
                    item.Cedula,
                    item.Sexo,
                    item.FechaNacimiento,
                    item.Ocupacion,
                    item.EstadoCivil,
                    DateTime.Now,
                    _usuario.Id,
                    DateTime.Now,
                    0);
            }
            if(item.Id>0) //PARA HACER UN UPDATE
            {
                Query.AppendFormat("UPDATE tblEstudiantes SET Nombre='{0}', Apellido='{1}', Sexo='{2}', Ocupacion='{3}', EstadoCivil='{4}', FechaNacimiento='{5}', Cedula='{6}', Modificado='{7}', ModificadoPorId={8} ",
                    item.Nombre,
                    item.Apellido,
                    item.Sexo,
                    item.Ocupacion,
                    item.EstadoCivil,
                    item.FechaNacimiento,
                    item.Cedula,
                    DateTime.Now,
                    _usuario.Id);
                Query.AppendFormat(" WHERE IdEstudiante={0}", item.Id);
            }
            try
            {
                Connection.Open(); //abreu la conexion a la base de datos
                Command = new SqlCommand(Query.ToString(), Connection); 
                if(Command.ExecuteNonQuery()>0)
                {
                    Respuesta.ID = 0;
                }
            }
            catch(Exception ex)
            {
                Respuesta.ID = 1;
                Respuesta.Descripcion = ex.Message;
            }
            finally
            {
                Connection.Close();
            }
        }

        /// <summary>
        /// Permite insertar o actualizar los datos de un estudiante.
        /// Utiliza un procedimiento almacenado.
        /// </summary>
        /// <param name="item">Un objeto tipo estudiante</param>
        public void GuardarEstudianteSP(Estudiante item)
        {
            LimpiarObjetos(); //Limpia los objetos que hagan falta.

            ///Los parámetros que se colocan para el stored procedure deben ir en el mismo orden como aparecen
            ///en el stored procedure creado en SQL SERVER
            Command.Connection = Connection;
            Command.CommandType = CommandType.StoredProcedure; //Indica que invocará un stored procedure de sql server
            Command.CommandText = "inscripcion_sp_GuardarEstudiante"; //aquí se coloca el nombre del stored procedure
            Command.Parameters.AddWithValue("@IdEstudiante", item.Id);
            Command.Parameters.AddWithValue("@Nombre", item.Nombre);
            Command.Parameters.AddWithValue("@Apellido", item.Apellido);
            Command.Parameters.AddWithValue("@Cedula", item.Cedula);
            Command.Parameters.AddWithValue("@FechaNacimiento", item.FechaNacimiento);
            Command.Parameters.AddWithValue("@Ocupacion", item.Ocupacion);
            Command.Parameters.AddWithValue("@Sexo", item.Sexo);
            Command.Parameters.AddWithValue("@EstadoCivil", item.EstadoCivil);
            Command.Parameters.AddWithValue("@IdUsuario", _usuario.Id);
            Command.Parameters.Add("@ret", SqlDbType.Int);
            Command.Parameters.Add("@newID", SqlDbType.Int);
            Command.Parameters["@ret"].Direction = ParameterDirection.Output; //Indica que este parametro es de salida.
            Command.Parameters["@newID"].Direction = ParameterDirection.Output;
            try
            {
                Connection.Open(); // abre la conexión a la base de datos
                Command.ExecuteNonQuery();
                if (Command.Parameters["@ret"].Value.ToString() == "0") //Verifica que el valor del parámetro de salida 
                {
                    Respuesta.ID = 0;  //Esto no es necesario porque ya se hizo en limpiar objetos
                    Respuesta.Descripcion = "OK";
                    Respuesta.NewID = Convert.ToInt32(Command.Parameters["@newID"].Value);
                }

            }
            catch (Exception ex)
            {
                Respuesta.ID = 1;
                Respuesta.Descripcion = ex.Message;
            }
            finally
            {
                Connection.Close();
            }
        }


        #endregion

        #region Profesores
        public DataTable GetListadoProfesores(string filtro)
        {
            StringBuilder sql = new StringBuilder("SELECT ");
            sql.Append(" IdProfesor, Nombre, Apellido, Cedula, Ocupacion, Sexo, FechaNacimiento [Nacimiento] FROM tblProfesores ");
            if (filtro.Length > 0)
                sql.AppendFormat(" WHERE {0}", filtro);
            return GetDataTableFromSQL(sql.ToString());
        }
        public DataTable GetListadoProfesoresSP(int IdProfesor)
        {
            return GetDataTableFromSQL(string.Format("EXEC inscripcion_sp_GetListadoProfesores {0}", IdProfesor));
        }



        #endregion

        #region Estado Civil

        public DataTable GetListadoEstadoCivil()
        {
            StringBuilder sql = new StringBuilder("SELECT Id, Nombre FROM tblEstadoCivil ");
            return GetDataTableFromSQL(sql.ToString());
        }
        public DataTable GetListadoAulas(int idCentro)
        {
            StringBuilder sql = new StringBuilder("SELECT Id, Codigo FROM tblAulas WHERE Inactivo=0 "); //Validar de que centro es el aula con el centro del estudiante
            sql.AppendFormat(" AND IdCentro={0}", idCentro);
            DataTable dt =  GetDataTableFromSQL(sql.ToString());
            if(dt!=null && dt.Rows.Count==0)
            {
                DataRow row = dt.NewRow();
                row["Id"] = 0;
                row["Codigo"] = "No hay aulas creadas.";
                dt.Rows.Add(row);
            }
            return dt;
         }
        public DataTable GetListadoProfesores()
        {
            StringBuilder sql = new StringBuilder("SELECT IdProfesor, Nombre FROM tblProfesores ");
            return GetDataTableFromSQL(sql.ToString());
        }
        public DataTable GetListadoAsignaturas(int idCarrera)
        {
            StringBuilder sql = new StringBuilder("SELECT Id, Descripcion FROM tblAsignaturas WHERE Inactivo=0 ");
            sql.AppendFormat(" AND IdCarrera={0}", idCarrera);
            DataTable dt = GetDataTableFromSQL(sql.ToString());
            if (dt != null && dt.Rows.Count == 0)
            {
                DataRow row = dt.NewRow();
                row["Id"] = 0;
                row["Descripcion"] = "No hay asignaturas creadas.";
                dt.Rows.Add(row);
            }
            return dt;
        }
        public DataTable GetListadoCentros()
        {
            StringBuilder sql = new StringBuilder("SELECT Id, Descripcion FROM tblCentros WHERE Inactivo=0 "); //hay que faltrarla por la carrera que tenga el estudiante
            return GetDataTableFromSQL(sql.ToString());
        }

        public DataTable GetListadoCarreras()
        {
            StringBuilder sql = new StringBuilder("SELECT Id, Nombre FROM tblCarreras WHERE Inactivo=0 "); //hay que faltrarla por la carrera que tenga el estudiante
            return GetDataTableFromSQL(sql.ToString());
        }


        #endregion


        #region Permisos



        public void FillPermisosListView(ListView listView)
        {
            Command.Connection = Connection;
            Command.CommandText = "SELECT Id, Nombre, IdPadre FROM tblPermisos WHERE Inactivo=0 ORDER BY  IdPadre asc, Id asc;";
            Command.CommandTimeout = 30;
            SqlDataReader r;
            try
            {
                Connection.Open();
                r = Command.ExecuteReader(); // para cargar el query en el data reader..
                if (r.HasRows)
                {

                    while (r.Read())
                    {
                        ListViewItem item = new ListViewItem(r.GetString(1), r.GetInt32(0).ToString());
                        listView.Items.Add(item);

                        // tambien se podria hacer de esta forma....
                        //listView1.Items.Add(new ListViewItem(r.GetString(1), r.GetInt32(0).ToString()));
                    }
                }
                r.Close(); //cerrar el data reader
                Respuesta.ID = 0;
                Respuesta.Descripcion = "OK";
            }
            catch (Exception ex)
            {
                Respuesta.ID = 1;
                Respuesta.Descripcion = ex.Message;
            }
            finally
            {
                Connection.Close();
            }

        }

        public void FillPermisosGroupListListView(ListView listView)
        {
            Command.Connection = Connection;
            Command.CommandText = "SELECT Id, Nombre, IdPadre FROM tblPermisos WHERE Inactivo=0 ORDER BY  IdPadre asc, Id asc;";
            Command.CommandTimeout = 30;
            SqlDataReader r;
            try
            {
                Connection.Open();
                r = Command.ExecuteReader();
                if (r.HasRows)
                {
                    while (r.Read())
                    {

                        ListViewGroup g = new ListViewGroup(r.GetInt32(0).ToString(), r.GetString(1));
                        ListViewItem i = new ListViewItem(r.GetString(1), r.GetInt32(0));
                        i.ImageKey = r.GetInt32(0).ToString();
                        if (r.GetInt32(2) == 0)
                        {
                            listView.Groups.Add(g);
                        }
                        else
                        {
                            i.Group = listView.Groups[r.GetInt32(2).ToString()];
                            listView.Items.Add(i);
                        }
                    }
                }
                r.Close();
                Respuesta.ID = 0;
                Respuesta.Descripcion = "OK";
            }
            catch (Exception ex)
            {
                Respuesta.ID = 1;
                Respuesta.Descripcion = ex.Message;
            }
            finally
            {
                Connection.Close();
            }

        }

        public void FillPermisosTreeView(TreeView treeView)
        {
            Command.Connection = Connection;
            Command.CommandText = "SELECT Id, Nombre, IdPadre FROM tblPermisos WHERE Inactivo=0 ORDER BY  IdPadre asc, Id asc;";
            Command.CommandTimeout = 30;
            SqlDataReader r;
            try
            {
                Connection.Open();
                r = Command.ExecuteReader();
                if (r.HasRows)
                {
                    while (r.Read())
                    {
                        TreeNode node = new TreeNode();
                        node.ImageKey = r.GetInt32(0).ToString();
                        node.Text = r.GetString(1);
                        node.Name = r.GetInt32(0).ToString();
                        if (treeView.Nodes[r.GetInt32(2).ToString()] == null)
                        {
                            //Esto verificara en los subnodos para agregar hijos a padres en subnodos
                            TreeNode[] n = treeView.Nodes.Find(r.GetInt32(2).ToString(), true);
                            if (n.Length > 0)
                            {
                                foreach (TreeNode x in n)
                                {
                                    x.Nodes.Add(node);
                                }
                            }
                            else
                                treeView.Nodes.Add(node);
                        }
                        else
                        {
                            treeView.Nodes[r.GetInt32(2).ToString()].Nodes.Add(node);
                        }
                    }
                }
                r.Close();
            }
            catch (Exception ex)
            {
                Respuesta.ID = 1;
                Respuesta.Descripcion = ex.Message;
            }
            finally
            {
                Connection.Close();
            }

        }

        public void UpdateTreeViewPermisos(TreeNodeCollection nodes, string permisos)
        {
            foreach (TreeNode node in nodes)
            {
                node.Checked = (permisos.Contains(";" + node.ImageKey + ";")) ? true : false;
                if (node.Nodes.Count > 0)
                    UpdateTreeViewPermisos(node.Nodes, permisos);
            }
        }


        #endregion

        #region Operaciones con Usuarios

        public DataTable GetListadoTipoUsuario(bool incluirTodos)
        {
            StringBuilder sql = new StringBuilder();
            if (incluirTodos)
                sql.Append("SELECT 0 [Id], 'Todos' [Nombre]  UNION ");

            sql.Append("SELECT Id, Nombre FROM tblTipoUsuario WHERE Inactivo=0 ORDER BY Id, Nombre ");

            return GetDataTableFromSQL(sql.ToString());
        }

        public void GuardarUsuario(Usuario item)
        {
            LimpiarObjetos(); //Limpia los objetos que hagan falta.

            ///Los parámetros que se colocan para el stored procedure deben ir en el mismo orden como aparecen
            ///en el stored procedure creado en SQL SERVER
            Command.Connection = Connection;
            Command.CommandType = CommandType.StoredProcedure; //Indica que invocará un stored procedure de sql server
            Command.CommandText = "inscripcion_sp_GuardarUsuario"; //aquí se coloca el nombre del stored procedure
            Command.Parameters.AddWithValue("@IdUsuario", item.Id);
            Command.Parameters.AddWithValue("@IdTipoUsuario", item.IdTipoUsuario);
            Command.Parameters.AddWithValue("@Nombre", item.Nombre);
            Command.Parameters.AddWithValue("@Username", item.Username);
            Command.Parameters.AddWithValue("@Password", item.Password);
            Command.Parameters.AddWithValue("@Permisos", item.Permisos);
            Command.Parameters.Add("@ret", SqlDbType.Int);
            Command.Parameters.Add("@newID", SqlDbType.Int);
            Command.Parameters["@ret"].Direction = ParameterDirection.Output; //Indica que este parametro es de salida.
            Command.Parameters["@newID"].Direction = ParameterDirection.Output;
            try
            {
                Connection.Open(); // abre la conexión a la base de datos
                Command.ExecuteNonQuery();
                if (Command.Parameters["@ret"].Value.ToString() == "0") //Verifica que el valor del parámetro de salida 
                {
                    Respuesta.ID = 0;  //Esto no es necesario porque ya se hizo en limpiar objetos
                    Respuesta.Descripcion = "OK";
                    Respuesta.NewID = Convert.ToInt32(Command.Parameters["@newID"].Value);
                }

            }
            catch (Exception ex)
            {
                Respuesta.ID = 1;
                Respuesta.Descripcion = ex.Message;
            }
            finally
            {
                Connection.Close();
            }
        }


        public Usuario GetUsuarioInfo(int Id)
        {
            Usuario item = new Usuario();
            Query.Clear(); //Limpia el stringuilder
            Query.AppendFormat("SELECT Nombre, Usuario, Permisos, IdTipoUsuario, Contrasena FROM tblUsuarios WHERE Id={0}", Id);
            try
            {
                Connection.Open();
                Command = new SqlCommand(Query.ToString(), Connection);
                SqlDataReader reader = Command.ExecuteReader(); //Para revisar los datos de la consulta es más fácil un DataReader
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        item.Nombre = reader["Nombre"].ToString();
                        item.Username = reader["Usuario"].ToString();
                        item.Password = reader["Contrasena"].ToString();
                        item.Permisos = reader["Permisos"].ToString();
                        item.IdTipoUsuario= Convert.ToInt32(reader["IdTipoUsuario"]); // reader.GetDateTime(5)
                    }
                }
            }
            catch (Exception ex)
            {
                Respuesta.ID = 1;
                Respuesta.Descripcion = ex.Message;
            }
            finally
            {
                Connection.Close();
            }

            return item;
        }

        public DataTable GetListadoUsuarios(string filtro)
        {
            StringBuilder sql = new StringBuilder("SELECT ");
            sql.Append(" u.Id [u.Id], u.Usuario, u.Nombre [u.Nombre], u.IdTipoUsuario [u.IdTipoUsuario], t.Nombre [t.Nombre] ");
            sql.Append(" FROM tblUsuarios u ");
            sql.Append(" INNER JOIN tblTipoUsuario t ON u.IdTipoUsuario=t.Id ");
            if (filtro.Length > 0)
                sql.AppendFormat(" WHERE {0} AND t.Inactivo=0", filtro);
            return GetDataTableFromSQL(sql.ToString());
        }


        public void CambiarContrasena(int IdUsuario, string password)
        {
            LimpiarObjetos();
            Query.AppendFormat("UPDATE tblUsuarios SET Contrasena='{0}' WHERE Id={1};", password, IdUsuario);
            try
            {
                Connection.Open(); //abreu la conexion a la base de datos
                Command = new SqlCommand(Query.ToString(), Connection);
                if (Command.ExecuteNonQuery() > 0)
                {
                    Respuesta.ID = 0;
                }
            }
            catch (Exception ex)
            {
                Respuesta.ID = 1;
                Respuesta.Descripcion = ex.Message;
            }
            finally
            {
                Connection.Close();
            }
        }


        /// <summary>
        /// Para verificar si el usuario existe.
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public Usuario GetUsuarioInfo(string username)
        {
            Usuario item = new Usuario();
            Query.Clear(); //Limpia el stringuilder
            Query.AppendFormat("SELECT Id, Contrasena FROM tblUsuarios WHERE UPPER(Usuario)='{0}'", username.ToUpper());
            try
            {
                Connection.Open();
                Command = new SqlCommand(Query.ToString(), Connection);
                SqlDataReader reader = Command.ExecuteReader(); //Para revisar los datos de la consulta es más fácil un DataReader
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        item.Id = Convert.ToInt32(reader["Id"].ToString());
                        item.Password= reader["Contrasena"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Respuesta.ID = 1;
                Respuesta.Descripcion = ex.Message;
            }
            finally
            {
                Connection.Close();
            }

            return item;
        }

        #endregion

        #region Operaciones con Tipo de Usuario

        public TipoUsuario GetTipoUsuarioInfo(int Id)
        {
            TipoUsuario item = new TipoUsuario();
            Query.Clear(); //Limpia el stringuilder
            Query.AppendFormat("SELECT Id, Nombre, Permisos FROM tblTipoUsuario WHERE Id={0}", Id);
            try
            {
                Connection.Open();
                Command = new SqlCommand(Query.ToString(), Connection);
                SqlDataReader reader = Command.ExecuteReader(); //Para revisar los datos de la consulta es más fácil un DataReader
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        item.Nombre = reader["Nombre"].ToString();
                        item.Permisos = reader["Permisos"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Respuesta.ID = 1;
                Respuesta.Descripcion = ex.Message;
            }
            finally
            {
                Connection.Close();
            }

            return item;
        }


        public void GuardarTipoUsuario(TipoUsuario item)
        {
            LimpiarObjetos(); //Limpia los objetos que hagan falta.

            ///Los parámetros que se colocan para el stored procedure deben ir en el mismo orden como aparecen
            ///en el stored procedure creado en SQL SERVER
            Command.Connection = Connection;
            Command.CommandType = CommandType.StoredProcedure; //Indica que invocará un stored procedure de sql server
            Command.CommandText = "inscripcion_sp_GuardarTipoUsuario"; //aquí se coloca el nombre del stored procedure
            Command.Parameters.AddWithValue("@Id", item.Id);
            Command.Parameters.AddWithValue("@Nombre", item.Nombre);
            Command.Parameters.AddWithValue("@Permisos", item.Permisos);
            Command.Parameters.Add("@ret", SqlDbType.Int);
            Command.Parameters.Add("@newID", SqlDbType.Int);
            Command.Parameters["@ret"].Direction = ParameterDirection.Output; //Indica que este parametro es de salida.
            Command.Parameters["@newID"].Direction = ParameterDirection.Output;
            try
            {
                Connection.Open(); // abre la conexión a la base de datos
                Command.ExecuteNonQuery();
                if (Command.Parameters["@ret"].Value.ToString() == "0") //Verifica que el valor del parámetro de salida 
                {
                    Respuesta.ID = 0;  //Esto no es necesario porque ya se hizo en limpiar objetos
                    Respuesta.Descripcion = "OK";
                    Respuesta.NewID = Convert.ToInt32(Command.Parameters["@newID"].Value);
                }

            }
            catch (Exception ex)
            {
                Respuesta.ID = 1;
                Respuesta.Descripcion = ex.Message;
            }
            finally
            {
                Connection.Close();
            }
        }
        public DataTable GetListadoTipoUsuarios(string filtro)
        {
            StringBuilder sql = new StringBuilder("SELECT ");
            sql.Append(" u.Id [u.Id], u.Nombre [u.Nombre], u.Permisos ");
            sql.Append(" FROM tblTipoUsuario u ");
            if (filtro.Length > 0)
                sql.AppendFormat(" WHERE {0} AND u.Inactivo=0", filtro);
            return GetDataTableFromSQL(sql.ToString());
        }

        #endregion
    }
}
