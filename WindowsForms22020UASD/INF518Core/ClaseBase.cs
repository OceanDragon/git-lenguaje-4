﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using INF518Core.Clases;
using System.Configuration;
using System.Data;

namespace INF518Core
{
    public class ClaseBase
    {
        private string strConn;
        public SqlConnection Connection { get; set; }  //Este objeto es necesario para cualquier intento de interacción con un servidor SQL SERVER
        public SqlCommand Command { get; set; } //guardar la conexión y el query que se le enviará al servidor
        public SqlDataAdapter Adapter { get; set; } //permite cargar los datos en un DataTable

        public Respuesta Respuesta { get; set; }
        public StringBuilder Query { get; set; }

        /// <summary>
        /// La cadena de conexión al servidor.
        /// </summary>
        public string CadenaConexion  
        {
            set
            {
                strConn = value;
            }
            get
            {
                return strConn;
            }

        }

        public ClaseBase()
        {
            //Hala los datos del archivo app.config, en la sección de conexionsString del app.config debe existir una con el nombre "conn"
            strConn = ConfigurationManager.ConnectionStrings["conn"].ToString();
            Connection = new SqlConnection(strConn);
            Command = new SqlCommand();
            Command.CommandTimeout = 30;
            Adapter = new SqlDataAdapter(Command);
            Respuesta = new Respuesta();
            Query = new StringBuilder();
        }


        /// <summary>
        /// Devuelve un Datatable con los datos del query que se le pasa como
        /// parámetro.
        /// Usar preferiblemente para queries tipo SELECT.
        /// </summary>
        /// <param name="sql">El sql con la cadena del query.</param>
        /// <returns>Un datatable con los datos del query.</returns>
        public DataTable GetDataTableFromSQL(string sql)
        {
            DataTable dt = new DataTable();
            try
            {
                Connection.Open();
                Adapter = new SqlDataAdapter(sql, Connection);
                Adapter.Fill(dt);
                Respuesta.ID = 0;
            }
            catch(Exception ex)
            {
                Respuesta.ID = 1;
                Respuesta.Descripcion = ex.Message;
            }
            finally
            {
                Connection.Close();
            }
            return dt;
        }

        /// <summary>
        /// Reinicia o limpia los objetos que pueden usarse en un query
        /// </summary>
        public void LimpiarObjetos()
        {
            Query.Clear();
            Command.Parameters.Clear();
            Respuesta.ID = 0;
            Respuesta.Descripcion = string.Empty;
            Respuesta.NewID = 0;
        }

    }
}
