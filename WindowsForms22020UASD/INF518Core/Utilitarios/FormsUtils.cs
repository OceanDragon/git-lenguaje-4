﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace INF518Core.Utilitarios
{
    public static class FormsUtils
    {
        public static void ShowWarningMessage(string mensaje)
        {
            MessageBox.Show(mensaje, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
        public static void ShowErrorMessage(string mensaje)
        {
            MessageBox.Show(mensaje, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public static bool Validar(this TextBox txt, bool? mostrarMensaje)
        {
            if(txt.Text.Trim().Length==0)
            {
                if (mostrarMensaje==true)
                {
                    ShowWarningMessage("Este campo no puede estar en blanco.");
                    txt.Focus();
                }
                return false;
            }
            return true;
        }

        public static string CheckTreeViewCheckedItems(this TreeNodeCollection nodes)
        {
            StringBuilder s = new StringBuilder();
            foreach (TreeNode node in nodes)
            {
                if (node.Checked)
                {
                    s.Append(node.ImageKey);
                    s.Append(";");
                }
                if (node.Nodes.Count > 0)
                    s.Append(CheckTreeViewCheckedItems(node.Nodes));
            }
            return s.ToString();
        }

        public static string GetListViewCheckedItems(this ListView listView)
        {
            StringBuilder permisos = new StringBuilder();
            foreach (ListViewItem item in listView.Items)
            {
                if (item.Checked)
                {
                    permisos.Append(item.ImageKey);
                    permisos.Append(";");
                }
            }
            return (permisos.Length>0 ? ";" : string.Empty) + permisos.ToString();
        }
    }
}
